import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import {UsersList} from "./components/UsersList";
import {Navbar} from "./components/Navbar";
import {AddUser} from "./components/AddUser";
import {EditUser} from "./components/EditUser";
import {Footer} from "./components/Footer";
import {ViewUser} from "./components/ViewUser";


function App() {


  return (
    <BrowserRouter>
      <Navbar/>
      <div className='container'/>
      <Switch>
        <Route path='/users/:id' component={ViewUser} />
        <Route path='/users' component={UsersList} />
        <Route path='/add' component={AddUser}/>
        <Route path='/edit/:id' component={EditUser} />

      </Switch>
      <Footer/>
    </BrowserRouter>
  );
}

export default App;