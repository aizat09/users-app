export interface iUser{
  id:number
  firstname:string
  lastname:string
  age: number
  gender: string
  username:string
  password: string
  email:string
}
export interface iAddUser{
  firstname:string
  lastname:string
  age: number
  gender: string
  username:string
  password: string
  email:string
}
export interface iEditUser{
  firstname:string
  lastname:string
  age: number
  gender: string
  username:string
  email:string
}