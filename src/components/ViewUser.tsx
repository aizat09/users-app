import React, {useEffect, useState} from "react";
import {useParams} from 'react-router'
import {iUser} from "../interfaces";
import {useHistory} from "react-router-dom";

const isUser = (obj: any): obj is iUser => typeof obj.age === 'string' && typeof obj.firstname === 'string'
const isUserArray = (obj: any): obj is iUser[] => Array.isArray(obj) && obj.every(isUser)

type ViewParam = { id: string };

export const ViewUser = () => {
  const {id} = useParams<ViewParam>()
  const [user, setUser] = useState<iUser>()

  const history = useHistory();

  const loc = (id: string) => {
    const row = JSON.parse(localStorage.getItem('users') || '[]')

    if (isUserArray(row)) {
      return row.find(u => u.id === Number(id)) as iUser
    }

    throw new Error("no user found")
  }
  useEffect(() => {
    setUser(() => loc(id))
    console.log(user);
  }, [id])

  return (
    <form className="add-page container" >
      <h4 className='underline center-align'> Show Page</h4>

      <label>First Name:
      <input type="text" name='firstname' value={user?.firstname}/>
      </label>

      <label>Last Name:
      <input type="text" name='lastname' value={user?.lastname}/>
      </label>

      <label>Age:
      <input type="number" name='age' value={user?.age} />
      </label>

      <label>Gender:
      <select className="browser-default" name='gender' value={user?.gender} >
        <option value="">Select...</option>
        <option value="male">Male</option>
        <option value="female">Female</option>
      </select>
      </label>

      <label>Username:
      <input type="text" name='username' value={user?.username}/>
      </label>

      <label>E-mail:
      <input type="email" name='email' value={user?.email}/>
      </label>

    </form>
  )
}
