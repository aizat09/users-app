import React, {useEffect, useState} from "react";
import {iUser} from "../interfaces";
import {NavLink} from 'react-router-dom'

const isUser = (obj: any): obj is iUser => typeof obj.gender === 'string' && typeof obj.firstname === 'string'
const isUserArray = (obj: any): obj is iUser[] => Array.isArray(obj) && obj.every(isUser)

export const UsersList = () => {

  const loc = () => {
    const row = JSON.parse(localStorage.getItem('users') || '[]')

    if (isUserArray(row)) {
      return row as iUser[]
    }
    return [] as iUser[]
  }
  const [users, setUsers] = useState<iUser[]>(loc())

  useEffect(() => {
    localStorage.setItem('users', JSON.stringify(users))
  }, [users])

  const deleteUser = (userId:number)=>{
    const newUsers = users.filter(user=> user.id !== userId)
      setUsers(newUsers)
  }

  if (users.length === 0) {
    return (<p>No Users</p>)
  }

  return (
    <div className="list-page container">
      <h4 className='underline center-align'>List of Users </h4>
      <ul>
        {
          users.map(user => {
            return (
              <li key={user.id}>
                <span>{user.id + ' '}  </span>
                <span>{user.firstname + ' ' + user.lastname + ' '}</span>
                <button>
                <NavLink className='materialize-icon black-text' to ={`/edit/${user.id}`} >edit </NavLink></button>
                <button>
                <NavLink className='materialize-icon green-text' to ={`/users/${user.id}`} >view </NavLink></button>
                <button
                 className='materialize-icon red-text' onClick={event => deleteUser(user.id)}>delete</button>
              </li>
            )
          })
        }
      </ul>
    </div>
  )
}