import React, {useEffect, useState} from "react";
import {iAddUser, iEditUser, iUser} from "../interfaces";
import {useForm} from "react-hook-form";
import {useParams} from 'react-router'
import {useHistory} from "react-router-dom";

type Params = { id: string }

const isUser = (obj: any): obj is iUser => typeof obj.age === 'string' && typeof obj.firstname === 'string'
const isUserArray = (obj: any): obj is iUser[] => Array.isArray(obj) && obj.every(isUser)

export const EditUser = () => {
  const {id} = useParams<Params>()
  const [user, setUser] = useState<iUser>()
  const [users, setUsers] = useState<iUser[]>([])

  const history = useHistory();
  const {register, handleSubmit, watch, errors, ...form} = useForm<iAddUser>()

  const loc = (id: string) => {
    const row: iUser[] = JSON.parse(localStorage.getItem('users') || '[]')

    if (isUserArray(row)) {
      setUsers(row)
      return row.find(u => u.id === Number(id)) as iUser
    }

    throw new Error("no user found")
  }

  useEffect(() => {
    const u = loc(id)
    setUser(u)
    form.reset(u)
  }, [id])

  useEffect(() => {
    localStorage.setItem('users', JSON.stringify(users))
  }, [users])

  const onSubmit = (data: iEditUser) => {
    let editUser: iUser = {
      id: Number(id),
      password: user!!.password,
      ...data
    }
    setUser(editUser)
    let newUsers: iUser[] = users.map(u => u.id === Number(id) ? {...editUser} : u)
    setUsers(newUsers)
    history.push('/users')
  }

  return (
    <form className="add-page container" onSubmit={handleSubmit(onSubmit)}>
      <h4 className='underline center-align'> Edit Page</h4>

      <label>First Name:
      <input type="text" name='firstname' id='firstname'
             ref={register({required: true, maxLength: 20, minLength: 2})}/>
      </label>
      {errors.firstname && <p>FirstName is required, maxLength: 20, minLength:2 </p>}

      <label >Last Name:
      <input type="text" name='lastname' id='lastname'ref={register({required: true, maxLength: 20, minLength: 2})}/>
      </label>
      {errors.lastname && <p>LastName is required, maxLength: 20, minLength:2 </p>}

      <label>Age:
      <input type="number" name='age'id='age' ref={register({min: 18, max: 99})}/>
      </label>
      {errors.age && <p>FirstName is required, min: 18, max: 99 </p>}

      <label>Gender:
      <select className="browser-default" name='gender' id='gender' ref={register({required: true})}>
        <option value="">Select...</option>
        <option value="male">Male</option>
        <option value="female">Female</option>
      </select>
      </label>
      {errors.gender && <p>LastName is required </p>}

      <label>Username:
      <input type="text" name='username' id='username' ref={register({required: true, maxLength: 20, minLength: 2})}/>
      </label>
      {errors.username && <p>LastName is required, maxLength: 20, minLength:2 </p>}

      <label>E-mail:
      <input type="email" name='email' id='email' ref={register({required: true})}/>
    </label>

      <br/>
      <input type="submit"/>

    </form>
  )
}