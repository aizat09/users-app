import React from "react";
import {NavLink} from 'react-router-dom'

export const Navbar = () => {


  return(
    <div className="navbar">
      <NavLink to='/add' className='underline black-text'>Add User</NavLink> <br/>
      <NavLink to='/users' className='underline black-text'>List of Users</NavLink>
    </div>
  )
}