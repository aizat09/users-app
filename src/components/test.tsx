import React, {useEffect, useState} from "react";
import {withRouter, useParams} from 'react-router'
import {iUser} from "../interfaces";

type ViewParam = { userId: string };

const isUser = (obj: any): obj is iUser => typeof obj.age === 'number' && typeof obj.firstname === 'string'
const isUserArray = (obj: any): obj is iUser[] => Array.isArray(obj) && obj.every(isUser)

const loc = () => {
  const row = JSON.parse(localStorage.getItem('users') || '[]')
  if (isUserArray(row)) {
    return row as iUser[]
  }
  return [] as iUser[]
}

export const ViewUser = () => {
  const {userId} = useParams<ViewParam>()
  const [user,setUser] = useState<iUser>()

  useEffect(()=>{
    const currentUser = loc()
      .find(u =>u.id === Number(userId) )

    if(!currentUser){
      return
    }

    setUser(currentUser)
  },[userId])


  return (
    <div className="add-page container">
      <h4 className='underline center-align'> User Info Page</h4>
      <label>First Name:</label>
      <input type="text" value={user?.firstname} name='firstname' className='center-align'/>

      <label>Last Name:</label>
      <input type="text"  name='lastname' className='center-align'/>
    </div>
  )
}
