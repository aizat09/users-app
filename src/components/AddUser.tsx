import React, {useEffect, useRef, useState} from "react";
import {useHistory} from "react-router-dom";
import {iAddUser, iUser} from "../interfaces";
import {useForm} from "react-hook-form";

const isUser = (obj: any): obj is iUser => typeof obj.gender === 'string' && typeof obj.firstname === 'string'
const isUserArray = (obj: any): obj is iUser[] => Array.isArray(obj) && obj.every(isUser)

export const AddUser = () => {

  const loc = () => {
    const row = JSON.parse(localStorage.getItem('users') || '[]')

    if (isUserArray(row)) {
      return row as iUser[]
    }
    return [] as iUser[]
  }
  const [users, setUsers] = useState<iUser[]>(loc())

  useEffect(() => {
    localStorage.setItem('users', JSON.stringify(users))
  }, [users])


  const history = useHistory();
  const {register, handleSubmit, watch, errors} = useForm<iAddUser>()

  const onSubmit = (data: iAddUser) => {
    let newUser: iUser = {
      id: Date.now(),
      ...data
    }
    console.log(users);
    setUsers([...users, newUser])
    history.push('/users')
  }

  return (
    <form className="add-form container" onSubmit={handleSubmit(onSubmit)}>
      <h4>Sign Up</h4>

      <label>First Name:
      <input type="text" name='firstname'  ref={register({ required: true, maxLength: 20, minLength:2 })}/>
      </label>
      {errors.firstname && <p>FirstName is required, maxLength: 20, minLength:2 </p>}


      <label>Last Name:
      <input type="text" name='lastname' ref={register({ required: true, maxLength: 20, minLength:2 })}/>
      </label>
      {errors.lastname && <p>LastName is required, maxLength: 20, minLength:2 </p>}


      <label>Age:
      <input type="number" name='age' ref={register({ min: 18, max: 99 })}/>
      </label>
      {errors.age && <p>Age is required, min: 18, max: 99 </p>}


      <label>Gender:
      <select className="browser-default" name='gender' ref={register({ required: true })}>
        <option value="" >Select...</option>
        <option value="male">Male</option>
        <option value="female">Female</option>
      </select>
      </label>
      {errors.gender && <p>Gender is required </p>}


      <label>Username:
      <input type="text" name='username' ref={register({ required: true, maxLength: 20, minLength:2 })}/>
      </label>
      {errors.username && <p>UserName is required, maxLength: 20, minLength:2 </p>}


      <label>Password:
      <input type="password" name='password' ref={register({ required: true, maxLength: 20, minLength:8 })}/>
      </label>
      {errors.password && <p>Password is required, maxLength: 20, minLength:8 </p>}


      <label>E-mail:
      <input type="email" name='email' ref={register({ required: true })}/>
      </label>

      <br/>
      <input type="submit"/>

    </form>
  )

}